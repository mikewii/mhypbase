#include "config.hpp"
#include "utils/utils.hpp"
#include "utils/print.hpp"

#include <filesystem>
#include <fstream>
#include <regex>
#include <SimpleIni.h>

namespace config {
static CSimpleIni ini;

static const char* client_version = nullptr;
static const char* config_channel = nullptr;
static const char* config_base_url = nullptr;
static const char* public_rsa_key = nullptr;
static const char* rsa_encrypt_key = nullptr;
static const char* private_rsa_key = nullptr;
static long magic_a = 0;
static long magic_b = 0;

auto GetEnableValue(const char* a_pKey, bool a_nDefault) -> bool
{
    return ini.GetBoolValue("Basic", a_pKey, a_nDefault);
}

auto GetLongValue(const char* a_pKey, long a_nDefault) -> long
{
    return ini.GetLongValue("Basic", a_pKey, a_nDefault);
}

auto GetMagicA(void) -> long
{
    return magic_a;
}

auto GetMagicB(void) -> long
{
    return magic_b;
}

auto GetOffsetValue(const char* a_pKey, long a_nDefault) -> long
{
    return ini.GetLongValue(client_version, a_pKey, a_nDefault);
}

auto GetAddress(uintptr_t baseAddress, const char* a_pKey, long a_nDefault) -> uintptr_t
{
    auto offset = GetOffsetValue(a_pKey, a_nDefault);

    if (offset == 0) {
        auto patternKey = std::string(a_pKey) + "_Pattern";
        auto pattern = ini.GetValue("Offset", patternKey.c_str(), nullptr);

        if (pattern != nullptr && strlen(pattern) > 0) {
            if (*pattern == '+') {
                pattern = pattern + 1;
                auto value = utils::FindEntry(utils::PatternScan("UserAssembly.dll", pattern));
                if (value)
                    offset = value - baseAddress;
            } else {
                auto value = utils::PatternScan("UserAssembly.dll", pattern);
                if (value)
                    offset = value - baseAddress;
            }
        }
    }

    if (offset) {
        print::log("[%s] %s = 0x%08X", client_version, a_pKey, offset);
    }

    return baseAddress + offset;
}

auto GetConfigChannel(void) -> const char*
{
    return config_channel;
}

auto GetConfigBaseUrl(void) -> const char*
{
    return config_base_url;
}

auto GetPublicRSAKey(void) -> const char*
{
    return public_rsa_key;
}

auto GetRSAEncryptKey(void) -> const char*
{
    return rsa_encrypt_key;
}

auto GetPrivateRSAKey(void) -> const char*
{
    return private_rsa_key;
}

auto Load(void) -> void
{
    ini.SetUnicode();
    ini.LoadFile(utils::GetConfigPath().c_str());

    if (GetEnableValue("EnableConsole", false))
        utils::InitConsole();

    client_version = ini.GetValue(  "Offset"
                                  , "ClientVersion"
                                  , nullptr);

    if (!client_version) {
        char filename[MAX_PATH] = {};
        GetModuleFileName(NULL, filename, MAX_PATH);
        auto path = std::filesystem::path(filename).parent_path() / "pkg_version";
        std::ifstream infile(path);
        std::string line;
        std::regex str_expr = std::regex("UserAssembly.dll.*\"([0-9a-f]{32})\"");
        auto match = std::smatch();

        while (std::getline(infile, line)) {
            std::regex_search(line, match, str_expr);

            if (match.size() == 2) {
                auto str_match = match[1].str();

                client_version = ini.GetValue(  "MD5ClientVersion"
                                              , str_match.c_str()
                                              , nullptr);

                print::log(  "Version detected %s"
                           , client_version ? client_version : "Offset");

                break;
            }
        }
    }

    magic_a = ini.GetLongValue(client_version, "magic_a", 0);
    magic_b = ini.GetLongValue(client_version, "magic_b", 0);
    config_channel = ini.GetValue("Value", "ConfigChannel", nullptr);
    config_base_url = ini.GetValue("Value", "ConfigBaseUrl", nullptr);
    public_rsa_key = ini.GetValue("Value", "PublicRSAKey", nullptr);
    rsa_encrypt_key = ini.GetValue("Value", "RSAEncryptKey", public_rsa_key);
    private_rsa_key = ini.GetValue("Value", "PrivateRSAKey", nullptr);
}

}; /// namespace config
