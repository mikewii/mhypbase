#include "il2cpp-init.hpp"

#include "config.hpp"
#include "hook.hpp"

#include "utils/utils.hpp"
#include "utils/print.hpp"

#include <chrono>

// bootleg cv
HANDLE userAssemblyEvent = NULL;
static constexpr auto waitLimit = std::chrono::seconds(60);

#if defined(__MINGW32__)
long CALLBACK ehandler(EXCEPTION_POINTERS *pointers) {
    switch (pointers->ExceptionRecord->ExceptionCode) {
    case EXCEPTION_STACK_OVERFLOW:
        return EXCEPTION_EXECUTE_HANDLER;
    default:
        return EXCEPTION_CONTINUE_SEARCH;
    }
};
#endif

DWORD WINAPI Thread(LPVOID /*p*/)
{
    config::Load();
    utils::DisableLogReport();

    auto res = WaitForSingleObject(  userAssemblyEvent
                                   , std::chrono::duration_cast<std::chrono::microseconds>(waitLimit).count());

    if (res != WAIT_OBJECT_0)
        return 1;

    print::log("Disabling vm protect.");
    utils::DisableVMProtect();
    print::log("Disabled vm protect.");

    print::log("Loading il2cpp functions.");
    init_il2cpp();
    print::log("Loaded il2cpp functions.");

    print::log("Loading hooks.");
    hook::Load();
    print::log("Loaded hooks.");

    return 0;
}

BOOL WINAPI DllMain(HINSTANCE hinstDLL,
                    DWORD fdwReason,
                    LPVOID /*lpvReserved*/)
{
    switch(fdwReason) {
    default:break;
    case DLL_PROCESS_ATTACH: {
        // Initialize once for each new process.
        // Return FALSE to fail DLL load.
        print::log("DLL_PROCESS_ATTACH");

        userAssemblyEvent = CreateEventA(  NULL
                                         , FALSE
                                         , FALSE
                                         , NULL);

        HANDLE hThread = CreateThread(  NULL
                                      , 0
                                      , Thread
                                      , hinstDLL
                                      , 0
                                      , NULL);

        if (hThread != NULL)
            CloseHandle(hThread);

        if (hinstDLL != NULL)
            DisableThreadLibraryCalls(hinstDLL);

        break;
    }
    case DLL_PROCESS_DETACH: {
        // Perform any necessary cleanup.
        print::log("DLL_PROCESS_DETACH");
        break;
    }
    case DLL_THREAD_ATTACH: {
        // Do thread-specific initialization.
        print::log("DLL_THREAD_ATTACH");

        static bool once = true;

        if (once) {
            auto dll = GetModuleHandleA("UserAssembly.dll");

            if (dll != NULL) {
                once = false;

                SetEvent(userAssemblyEvent);
            }
        }

        break;
    }
    case DLL_THREAD_DETACH: {
        // Do thread-specific cleanup.
        print::log("DLL_THREAD_DETACH");
        break;
    }
    };

    return TRUE;
}
