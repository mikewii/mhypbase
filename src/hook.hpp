#pragma once
#include <windows.h>
#include <string>

namespace hook {
extern auto uint64_to_hex_string(uint64_t value) -> std::string;
extern auto MiHoYo__SDK__SDKUtil_RSAEncrypt(LPVOID publicKey, LPVOID content) -> LPVOID;
extern auto MoleMole__MoleMoleSecurity_GetPublicRSAKey(void) -> LPVOID;
extern auto MoleMole__MoleMoleSecurity_GetPrivateRSAKey(void) -> LPVOID;
extern auto MoleMole__RSAUtil_RSAEncrypt(LPVOID key, LPVOID data) -> LPVOID;
extern auto MoleMole__RSAUtil_RSADecrypt(LPVOID key, LPVOID data) -> LPVOID;
extern auto MoleMole__RSAUtil_RSAVerifyHash(LPVOID key, LPVOID bytes, LPVOID sign) -> LPVOID;
extern auto MoleMole__RSAUtil_RSAVerifyData(LPVOID key, LPVOID bytes, LPVOID sign) -> LPVOID;
extern auto TryPatchConfig(std::string text) -> std::string;
extern auto UnityEngine__JsonUtility_FromJson(LPVOID json, LPVOID type, LPVOID method) -> LPVOID;
extern auto MoleMole__ConfigUtil_LoadJSONStrConfig(LPVOID jsonText, LPVOID useJsonUtility, LPVOID method) -> LPVOID;
extern auto MoleMole__FightModule_OnWindSeedClientNotify(LPVOID __this, LPVOID notify) -> LPVOID;
extern auto MoleMole__PlayerModule_OnWindSeedClientNotify(LPVOID __this, LPVOID notify) -> LPVOID;
extern auto MoleMole__PlayerModule_OnReciveLuaShell(LPVOID __this, LPVOID notify) -> LPVOID;
extern auto Load(void) -> void;
}; /// namespace hook
