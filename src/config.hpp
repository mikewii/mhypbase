#pragma once
#include <cstdint>

namespace config {
extern auto GetEnableValue(const char* a_pKey, bool a_nDefault) -> bool;
extern auto GetLongValue(const char* a_pKey, long a_nDefault) -> long;
extern auto GetMagicA(void) -> long;
extern auto GetMagicB(void) -> long;
extern auto GetOffsetValue(const char* a_pKey, long a_nDefault) -> long;
extern auto GetAddress(uintptr_t baseAddress, const char* a_pKey, long a_nDefault) -> uintptr_t;
extern auto GetConfigChannel(void) -> const char*;
extern auto GetConfigBaseUrl(void) -> const char*;
extern auto GetPublicRSAKey(void) -> const char*;
extern auto GetRSAEncryptKey(void) -> const char*;
extern auto GetPrivateRSAKey(void) -> const char*;
extern auto Load(void) -> void;
}; /// namespace config
