#pragma once
#include <iostream>
#include <map>
#include <windows.h>
#include "detours.h"

#ifndef __FUNCTION_NAME__
#ifdef WIN32   //WINDOWS
#define __FUNCTION_NAME__   __FUNCTION__
#else          //*NIX
#define __FUNCTION_NAME__   __func__
#endif
#endif

#define CALL_ORIGIN(function, ...) HookManager::call(function, __FUNCTION_NAME__, ##__VA_ARGS__)

class HookManager
{
public:
    template <typename Fn>
    static auto install(Fn func, Fn handler) -> void
    {
        enable(func, handler);
        holderMap[reinterpret_cast<void*>(handler)] = reinterpret_cast<void*>(func);
    }

    template <typename Fn>
    static auto getOrigin(Fn handler, const char* callerName = nullptr) noexcept -> Fn
    {
        if (holderMap.count(reinterpret_cast<void*>(handler)) == 0) {
            std::cout << "Origin not found for handler: "
                      << callerName
                      << ". Maybe racing bug."
                      << std::endl;

            return nullptr;
        }
        return reinterpret_cast<Fn>(holderMap[reinterpret_cast<void*>(handler)]);
    }

    template <typename Fn>
    static auto detach(Fn handler) noexcept -> void
    {
        disable(handler);
        holderMap.erase(reinterpret_cast<void*>(handler));
    }

    template <typename RType, typename... Params>
    static auto call(RType(*handler)(Params...), const char* callerName = nullptr, Params... params) -> RType
    {
        auto origin = getOrigin(handler, callerName);

        if (origin != nullptr)
            return origin(params...);

        return RType();
    }

    static auto detachAll(void) noexcept -> void
    {
        for (const auto& [key, value] : holderMap)
            disable(key);

        holderMap.clear();
    }

private:
    inline static std::map<void*, void*> holderMap{};

    template <typename Fn>
    static auto disable(Fn handler) -> void
    {
        Fn origin = getOrigin(handler);
        DetourTransactionBegin();
        DetourUpdateThread(GetCurrentThread());
        DetourDetach(&(PVOID&)origin, handler);
        DetourTransactionCommit();
    }

    template <typename Fn>
    static auto enable(Fn& func, Fn handler) -> void
    {
        DetourTransactionBegin();
        DetourUpdateThread(GetCurrentThread());
        DetourAttach(&(PVOID&)func, (PVOID)handler);
        DetourTransactionCommit();
    }
};
