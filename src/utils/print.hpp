#pragma once
#include <cstddef>

namespace print {
extern auto hexDump(const void* data,
                    const std::size_t size) noexcept -> void;

extern auto log(const char* fmt, ...) noexcept -> void;
}; /// namespace print
