#include "print.hpp"
#include <cstdarg>
#include <iostream>
#include <iomanip>
#include <windows.h>
#include <mutex>

namespace print {

std::mutex printMutex;

auto hexDump(const void* data, const std::size_t size) noexcept -> void
{
    std::lock_guard<std::mutex> lock(printMutex);

    const uint8_t* p = static_cast<const uint8_t*>(data);
    const auto flags{std::cout.flags()};

    std::cout << "[hex dump] size: " << size << " addr: "
              << std::setw(8)
              << std::setfill('0')
              << std::hex
              << std::uppercase
              << reinterpret_cast<std::size_t>(data)
              << '\n';

    for (auto i = 0u; i < size; i++) {
        if (i > 0) {
            if (i % 16 == 0)
                std::cout << '\n';
            else if (i % 4 == 0)
                std::cout << "  ";
        }

        std::cout << std::setw(2)
                  << std::setfill('0')
                  << std::hex
                  << std::uppercase
                  << static_cast<int>(p[i])
                  << ' ';
    }

    std::cout.flags(flags);
    std::cout << std::endl;
}

auto log(const char* fmt, ...) noexcept -> void
{
    std::lock_guard<std::mutex> lock(printMutex);

    va_list args;

    fprintf(stdout, "[mhypbase] ");

    va_start(args, fmt);
    vfprintf(stdout, fmt, args);
    va_end(args);

    fprintf(stdout, "\n");

    fflush(stdout);
}

}; /// namespace print
