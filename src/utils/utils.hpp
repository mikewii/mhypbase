#pragma once
#include <windows.h>
#include <string>

namespace utils {
extern auto GetSelfModuleHandle(void) -> HMODULE;
extern auto GetConfigPath(void) -> std::string;
extern auto ConvertToString(VOID* ptr) -> std::string;
extern auto InitConsole(void) -> void;
extern auto DisableLogReport(void) -> void;
extern auto DisableVMProtectOLD(void) -> void;
extern auto DisableVMProtect(void) -> void;
extern auto FindEntry(uintptr_t addr) -> uintptr_t;
extern auto PatternScan(LPCSTR module, LPCSTR pattern) -> uintptr_t;
extern auto DumpAddress(uint32_t start, long magic_a, long magic_b) -> void;
}; /// namespace utils
